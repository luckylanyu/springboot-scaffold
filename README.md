## 脚手架介绍：
1. springboot-web项目
2. 依赖管理与项目编译：maven
3. 持久层框架：mybatis
4. 数据库连接池：druid
5. 数据库：mysql
6. 插件：
    * 通用mapper
    * lombok
    * github.pagehelper
7. 缓存机制：redis
8. 日志记录：logback

## 启动要求：
1. 安装mysql数据库，执行项目sql/init.sql脚本
2. 安装redis数据库，不设置密码。


## 示例的功能：
1. 使用RSA算法，前端获取后台的公钥来加密登录密码，后台使用私钥解密。详见login.html
2. Excel导入导出功能，使用jxls实现。参考报关单编辑页面的表体导入功能。
3. 模拟网页登录。使用HTTPClient手动登录。
4. 代码自动生成。Mybatis及MybatisPlus框架。
5. xml报文解析及生成服务。
6. 多数据源功能。
7. 过滤器对动态ip黑名单、动态url白名单的控制功能。通过读取windows、linux系统环境变量。实现在应用页面中控制环境变量的动态改变。
8. Json Web Token(JWT)的生成和解析。
9. 使用WebSocket协议主动向客户端某用户推送消息。事件监听器、广播。
10. 事件监听器的应用。
11. 使用JasperReport技术实现打印预览及下载PDF文件
12. 数据库记录的新增、修改时，对固定字段 create_by,create_name,create_time,update_by,update_name,update_time的切面统一处理。



  