CREATE DATABASE IF NOT EXISTS `test`;

-- 用户信息表
DROP TABLE IF EXISTS `test`.`user_info`;
CREATE TABLE `test`.`user_info` (
  `id` varchar(36) NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `salt` varchar(255) NOT NULL COMMENT '密码盐',
  `phone` smallint DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息表';

-- 报关单表头
DROP TABLE IF EXISTS `test`.`entry_head`;
CREATE TABLE `test`.`entry_head` (
  `ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SEQ_NO` varchar(32) DEFAULT NULL COMMENT '单据编号',
  `ETPS_PREENT_NO` varchar(32) DEFAULT NULL COMMENT '预录入编号',
  `AGENT_CODE` varchar(10) DEFAULT NULL COMMENT '申报单位代码',
  `AGENT_NAME` varchar(70) DEFAULT NULL COMMENT '申报单位名称',
  `BILL_NO` varchar(32) DEFAULT NULL COMMENT '提单号',
  `CONTRACT_NO` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '合同号',
  `I_E_DATE` datetime DEFAULT NULL COMMENT '进出口日期',
  `DCL_TIME` datetime DEFAULT NULL COMMENT '申报时间',
  `CREATE_BY` varchar(64) DEFAULT NULL COMMENT '创建人代码',
  `CREATE_NAME` varchar(64) DEFAULT NULL COMMENT '创建人名称',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` varchar(64) DEFAULT NULL COMMENT '修改人代码',
  `UPDATE_NAME` varchar(64) DEFAULT NULL COMMENT '修改人名称',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  `CONTAINER_NUM` int DEFAULT '0' COMMENT '集装箱数',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报关单表头';

INSERT INTO `test`.`entry_head` VALUES ('2c92808f6b253c6c016b2aad0d5b1a7b', '201906061024310001', '54125421451', '2201221979', '广东新宝电器股份有限公司', '54141281', 'CG-20190605-645418', '2019-06-11 15:25:31', '2019-06-07 16:28:21', '13509961314', 'xgl', '2019-06-06 10:45:13', '13509961314', 'xgl', '2021-06-18 15:41:18', '20');
INSERT INTO `test`.`entry_head` VALUES ('2c92808f6b253c6c016b2aad0d5b1a7c', '201906061024310002', '54125421452', '2201221979', '广东新宝电器股份有限公司', '54141245', 'CG-20190605-112541', '2019-06-12 23:18:39', '2019-06-06 11:37:32', '13509961314', 'xgl', '2019-06-06 10:45:13', '13509961314', 'xgl', '2019-06-06 10:45:13', '25');


-- 报关单商品表
DROP TABLE IF EXISTS `test`.`entry_list`;
CREATE TABLE `test`.`entry_list` (
  `ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SEQ_NO` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '单据编号，关联键',
  `G_NO` decimal(19,0) DEFAULT NULL COMMENT '商品序号',
  `CODE_T_S` varchar(10) DEFAULT NULL COMMENT '商品编号',
  `G_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `G_MODEL_NAME` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品规格、型号',
  `DECL_PRICE` decimal(19,5) DEFAULT NULL COMMENT '申报单价',
  `DECL_TOTAL` decimal(19,5) DEFAULT NULL COMMENT '申报总价',
  `G_QTY` decimal(19,5) DEFAULT NULL COMMENT '申报数量',
  `PRODUCE_DATE` varchar(2000) DEFAULT NULL COMMENT '生产日期',
  `GROSS_WET` decimal(19,5) DEFAULT NULL COMMENT '毛重',
  `NET_WT` decimal(19,5) DEFAULT NULL COMMENT '净重',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='进口/出口报关单表体';

INSERT INTO `test`.`entry_list` VALUES ('2c92808f6b253c6c016b2aad0d5b1a7a', '201906061024310001', '1', '8516605124', '电热锅', '国外1822W', '39.27000', '45874.40000', '600.00000', null, '1552.00000', '1550.00000');
INSERT INTO `test`.`entry_list` VALUES ('2c92808f6b253c6c016b2aad0d5b1a7b', '201906061024310001', '2', '8516605122', '电风扇', '国外200W', '19.48000', '174.60000', '300.00000', null, '625.00000', '620.00000');
INSERT INTO `test`.`entry_list` VALUES ('2c92808f6b253c6c016b2aad0d5b1a7d', '201906061024310002', '1', '8516722000', '多士炉', '国外3000W', '24.89000', '64913.12000', '2608.00000', null, '8867.20000', '6650.40000');
INSERT INTO `test`.`entry_list` VALUES ('2c92808f6b253c6c016b2aad0d5b1a7e', '201906061024310002', '2', '8516605000', '电烤箱', '国外2000W', '69.27000', '49874.40000', '720.00000', null, '8424.00000', '7344.00000');


-- 定时任务信息表
DROP TABLE IF EXISTS `test`.`scheduled_biz_task_info`;
CREATE TABLE `test`.`scheduled_biz_task_info` (
  `id` varchar(36) NOT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `rate_seconds` smallint DEFAULT NULL COMMENT '任务间隔周期，单位为秒',
  `status` varchar(1) DEFAULT NULL COMMENT '任务状态。N-失效，Y-有效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='定时任务信息表';

INSERT INTO `test`.`scheduled_biz_task_info` VALUES ('1', 'task1', '5', 'Y');
INSERT INTO `test`.`scheduled_biz_task_info` VALUES ('2', 'task2', '6', '');


