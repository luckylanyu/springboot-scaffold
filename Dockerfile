FROM java:8
MAINTAINER yule <1063097422@qq.com>
VOLUME /tmp
ADD springboot_frame_demo-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]