let clickSubmit = () => {
    getPublicKey().then(res => {
        doSubmit(res)
    }).catch(err => {
        layer.msg('获取publicKey失败！'+err, {icon: 2, time: 1500})
    })
}

let doSubmit = res => {
    // console.info('start dosubmit')
    let userName = $("#userName").val();
    let password = $("#password").val()
    let encryptedPwd = ''
    //1.非空校验
    if (!userName) {
        flashRed("userName");//输入框闪动效果
        $("#userName").focus();
        return false;
    }
    if (!password) {
        flashRed("password");//输入框闪动效果
        $("#password").focus();
        return false;
    }
    //2.对密码输入框进行加密
    if (password.length !== 256) {
        let modulus = res.data.modulus;
        let exponent = res.data.exponent;
        //根据modulus+exponent获得公钥
        let publicKey = RSAUtils.getKeyPair(exponent, '', modulus);
        //使用公钥加密原密码
        encryptedPwd = RSAUtils.encryptedString(publicKey, password);
        //反填到页面上
        $('#password').val(encryptedPwd);
    }
    //3.发送后台请求进行登录
    doLogin({userName: userName, password: encryptedPwd}).then(res => {
        let resCode = res.code
        if (resCode === 1) {
            layer.msg('登录成功！', {icon: 1, time: 1500}, function () {
                window.location.href = "./index.html";
            })
        } else {
            layer.msg('登录失败！' + JSON.stringify(res.data), {icon: 2, time: 1500}, function () {
                window.location.reload();
            })
        }
    }).catch(err => {
        layer.msg('登录失败！' + JSON.stringify(err), {icon: 2, time: 1500})
    })

}
let doLogin = param => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/demo/login?t=" + Math.floor(Math.random() * 100),
            type: "POST",
            data: param,
            dataType: 'json',
            crossDomain: true == !(document.all),
            success: result => {
                resolve(result);
            },
            error: err => {
                reject(err);
            }
        })
    })
}
//回车键绑定登录按钮
$(document).keyup(function (event) {
    if (event.keyCode == 13) {
        $("#submit").click();
    }
});