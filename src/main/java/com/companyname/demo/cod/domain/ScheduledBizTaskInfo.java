package com.companyname.demo.cod.domain;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * 定时业务任务信息表
 * </p>
 *
 * @author XGL
 * @since 2021-06-09
 */
@Table(name = "scheduled_biz_task_info")
@Data
public class ScheduledBizTaskInfo {

    @Id
    private String id;
    private String taskName;
    /**
     * 任务间隔周期，单位为秒
     */
    private Integer rateSeconds;
    /**
     * 任务状态。N-失效，Y-有效
     */
    private String status;


}
