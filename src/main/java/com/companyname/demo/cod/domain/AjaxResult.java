package com.companyname.demo.cod.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class AjaxResult {
    public enum Type {
        ERROR(0, "error"),
        SUCCESS(1, "success"),
        UNAUTHORIZED(401, "unauthorized"),
        INVALID_TOKEN(601, "invalidToken"),//不合法的token
        OTHER_CLIENT_LOGIN(602, "otherClientLogin"),//异地登录被挤下线
        TOKEN_TIMEOUT(603, "tokenTimeout"),//token超时失效

        ;

        private int code;
        private String desc;

        Type(int code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public int getCode() {
            return this.code;
        }

        public String getDesc() {
            return this.desc;
        }
    }

    private int code;
    private String message;
    private Object data;

    public AjaxResult(Type type) {
        this.code = type.getCode();
        this.message = type.getDesc();
    }

    public AjaxResult(Type type, Object data) {
        this.code = type.getCode();
        this.message = type.getDesc();
        this.data = data;
    }

    public static AjaxResult success(Object data) {
        return new AjaxResult(Type.SUCCESS, data);
    }

    public static AjaxResult error() {
        return new AjaxResult(Type.ERROR);
    }

    public static AjaxResult error(Object data) {
        return new AjaxResult(Type.ERROR, data);
    }

    public static AjaxResult error401(Object data) {
        return new AjaxResult(Type.UNAUTHORIZED, data);
    }

    public static AjaxResult error601(Object data) {
        return new AjaxResult(Type.INVALID_TOKEN, data);
    }

    public static AjaxResult error602(Object data) {
        return new AjaxResult(Type.OTHER_CLIENT_LOGIN, data);
    }

    public static AjaxResult error603(Object data) {
        return new AjaxResult(Type.TOKEN_TIMEOUT, data);
    }
}
