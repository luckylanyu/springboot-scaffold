package com.companyname.demo.cod.domain;

import lombok.Data;

import javax.persistence.Transient;

@Data
public class BaseEntity {

    @Transient
    private Integer pageNum = 0;

    @Transient
    private Integer pageSize = 10;

    @Transient
    private String orderBy;
}
