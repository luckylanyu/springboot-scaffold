package com.companyname.demo.cod.domain;

import java.math.BigDecimal;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@Table(name = "entry_list")
public class EntryList {
    @Id
    @Column(name = "ID")
    private String id;

    /**
     * 单据编号，关联键
     */
    @Column(name = "SEQ_NO")
    private String seqNo;

    /**
     * 商品序号
     */
    @Column(name = "G_NO")
    @JsonProperty("gNo")//该注解解决驼峰命名失效问题
    private BigDecimal gNo;

    /**
     * 商品编号
     */
    @Column(name = "CODE_T_S")
    private String codeTS;

    /**
     * 商品名称
     */
    @Column(name = "G_NAME")
    @JsonProperty("gName")
    private String gName;

    /**
     * 商品规格、型号
     */
    @Column(name = "G_MODEL_NAME")
    @JsonProperty("gModelName")
    private String gModelName;

    /**
     * 申报单价
     */
    @Column(name = "DECL_PRICE")
    private BigDecimal declPrice;

    /**
     * 申报总价
     */
    @Column(name = "DECL_TOTAL")
    private BigDecimal declTotal;

    /**
     * 申报数量
     */
    @Column(name = "G_QTY")
    @JsonProperty("gQty")
    private BigDecimal gQty;

    /**
     * 生产日期
     */
    @Column(name = "PRODUCE_DATE")
    private String produceDate;

    /**
     * 毛重
     */
    @Column(name = "GROSS_WET")
    private BigDecimal grossWet;

    /**
     * 净重
     */
    @Column(name = "NET_WT")
    private BigDecimal netWt;
}