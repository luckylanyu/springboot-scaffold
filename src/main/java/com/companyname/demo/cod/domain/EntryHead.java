package com.companyname.demo.cod.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "entry_head")
public class EntryHead {

    @Id
    @Column(name = "ID")
    private String id;

    /**
     * 单据编号
     */
    @Column(name = "SEQ_NO")
    private String seqNo;

    /**
     * 预录入编号
     */
    @Column(name = "ETPS_PREENT_NO")
    private String etpsPreentNo;

    /**
     * 申报单位代码
     */
    @Column(name = "AGENT_CODE")
    private String agentCode;

    /**
     * 申报单位名称
     */
    @Column(name = "AGENT_NAME")
    private String agentName;

    /**
     * 提单号
     */
    @Column(name = "BILL_NO")
    private String billNo;

    /**
     * 合同号
     */
    @Column(name = "CONTRACT_NO")
    private String contractNo;

    /**
     * 进出口日期
     */
    @Column(name = "I_E_DATE")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //该JsonFormat注解将数据库datetime类型的字段自动转换为指定格式的Date类型，timezone根据情况选择GMT或GMT+8
    private Date iEDate;

    /**
     * 申报时间
     */
    @Column(name = "DCL_TIME")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dclTime;

    /**
     * 创建人代码
     */
    @Column(name = "CREATE_BY")
    private String createBy;

    /**
     * 创建人名称
     */
    @Column(name = "CREATE_NAME")
    private String createName;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    private Date createTime;

    /**
     * 修改人代码
     */
    @Column(name = "UPDATE_BY")
    private String updateBy;

    /**
     * 修改人名称
     */
    @Column(name = "UPDATE_NAME")
    private String updateName;

    /**
     * 修改时间
     */
    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    /**
     * 集装箱数
     */
    @Column(name = "CONTAINER_NUM")
    private Integer containerNum;
}