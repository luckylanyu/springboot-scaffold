package com.companyname.demo.cod.domain;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 用户信息表
 * </p>
 * @author XGL
 * @since 2021-06-03
 */
@Table(name = "user_info")
@Data
public class UserInfo implements Serializable{
	private static final long serialVersionUID = -8599244582350243211L;
	@Id
	private String id;
	private String userName;
	private String password;
	private String salt;
	private Integer phone;
	private String email;

}
