package com.companyname.demo.cod.util;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListUtil {

    /**
     * 新旧集合比较，获得新增的元素
     */
    public static List<Object> getNewsFromNewList(List<Object> oldList, List<Object> newList) {
        List<Object> oldListCopy = new ArrayList<>(oldList);
        List<Object> newListCopy = new ArrayList<>(newList);
        //求交集，会修改oldListCopy
        oldListCopy.retainAll(newList);
        newListCopy.removeAll(oldListCopy);//减去交集就是新增的元素
        List<Object> newEles = newListCopy;
        return newEles;
    }

    public static void main(String[] args) {
        /***************求新增元素***************/
        List<Object> list1 = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5"));
        List<Object> list2 = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5", "6", "7"));
        List<Object> new1 = getNewsFromNewList(list1, list2);
        System.out.println("new1 = " + new1);

        List<Object> list3 = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5"));
        List<Object> list4 = new ArrayList<>(Arrays.asList("1", "2", "6"));
        List<Object> new2 = getNewsFromNewList(list3, list4);
        System.out.println("new2 = " + new2);

        List<Object> list5 = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5"));
        List<Object> list6 = new ArrayList<>(Arrays.asList("2", "4", "6"));
        List<Object> new3 = getNewsFromNewList(list5, list6);
        System.out.println("new3 = " + new3);

        List<Object> list7 = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5"));
        List<Object> list8 = new ArrayList<>(Arrays.asList("6", "7", "8"));
        List<Object> new4 = getNewsFromNewList(list7, list8);
        System.out.println("new4 = " + new4);


    }

}
