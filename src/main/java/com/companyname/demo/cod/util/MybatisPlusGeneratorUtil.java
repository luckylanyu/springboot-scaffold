package com.companyname.demo.cod.util;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @Description:使用MybatisPlus工具自动生成代码，服务入口
 * @Author: XGL
 * @Date: 2019/6/29 12:06
 */
public class MybatisPlusGeneratorUtil {
    public static void main(String[] args) {
        //用来获取mybatisplus.properties文件的配置信息
        final ResourceBundle resourceBundle = ResourceBundle.getBundle("mybatisplus-generator");

        AutoGenerator autoGenerator = new AutoGenerator();
        // 全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setOutputDir(resourceBundle.getString("OutputDir"));
        globalConfig.setFileOverride(true);
        globalConfig.setActiveRecord(true);
        globalConfig.setEnableCache(false);
        globalConfig.setBaseResultMap(true);
        globalConfig.setBaseColumnList(true);
        globalConfig.setAuthor(resourceBundle.getString("author"));
        globalConfig.setMapperName("%sMapper");//xxxMapper.java，%s 会自动填充表实体属性
        autoGenerator.setGlobalConfig(globalConfig);

        // 数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        //ORACLE数据库
        //dataSourceConfig.setDbType(DbType.ORACLE);
        //dataSourceConfig.setDriverName("oracle.jdbc.driver.OracleDriver");
        /*dataSourceConfig.setTypeConvert(new OracleTypeConvert(){
            // 自定义数据库表字段类型转换
            @Override
            public DbColumnType processTypeConvert(String fieldType) {
                return super.processTypeConvert(fieldType);
            }
        });*/

        //SQL_SERVER数据库
        //dataSourceConfig.setDbType(DbType.SQL_SERVER);
        //dataSourceConfig.setDriverName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        /*dataSourceConfig.setTypeConvert(new SqlServerTypeConvert(){
            // 自定义数据库表字段类型转换
            @Override
            public DbColumnType processTypeConvert(String fieldType) {
                return super.processTypeConvert(fieldType);
            }
        });*/

        //MYSQL数据库
        dataSourceConfig.setDbType(DbType.MYSQL);
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setTypeConvert(new MySqlTypeConvert(){
            // 自定义数据库表字段类型转换
            @Override
            public DbColumnType processTypeConvert(String fieldType) {
                return super.processTypeConvert(fieldType);
            }
        });

        dataSourceConfig.setUrl(resourceBundle.getString("datasource_url"));
        dataSourceConfig.setUsername(resourceBundle.getString("datasource_userName"));
        dataSourceConfig.setPassword(resourceBundle.getString("datasource_password"));
        autoGenerator.setDataSource(dataSourceConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);// 表名生成策略:下划线变驼峰
        strategy.setInclude(new String[] {resourceBundle.getString("tableName")}); // 需要生成的表
        autoGenerator.setStrategy(strategy);

        // 包配置
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent(resourceBundle.getString("package_parent"));
        autoGenerator.setPackageInfo(packageConfig);

        // 指定xml文件名及生成路径
        InjectionConfig injectionConfig = new InjectionConfig() {
            @Override
            public void initMap() {

            }
        };
        List<FileOutConfig> focList = new ArrayList<FileOutConfig>();
        focList.add(new FileOutConfig(" ") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return resourceBundle.getString("OutputDirXml") + tableInfo.getEntityName() + "Mapper.xml";
            }
        });
        injectionConfig.setFileOutConfigList(focList);
        autoGenerator.setCfg(injectionConfig);

        autoGenerator.execute();
    }
}
