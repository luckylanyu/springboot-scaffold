package com.companyname.demo.cod.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringUtil implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringUtil.applicationContext == null) {
            SpringUtil.applicationContext = applicationContext;
        }
    }

    /**
     * 获取Spring容器
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /***
     * 根据Bean name获取容器中指定Bean组件
     */
    public static Object getBean(String name) {
        return getApplicationContext().getBean(name);
    }

    /***
     * 根据Bean class获取容器中指定Bean组件
     */
    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    /***
     * 根据Bean name+class获取容器中指定Bean组件
     */
    public static <T> T getBean(String name, Class<T> clazz) {
        return getApplicationContext().getBean(name, clazz);
    }


}