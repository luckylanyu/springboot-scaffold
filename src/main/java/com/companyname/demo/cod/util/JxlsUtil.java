package com.companyname.demo.cod.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class JxlsUtil {

    public static class OwnerOutputStream extends OutputStream{
        private ZipOutputStream zipOutputStream;
        public OwnerOutputStream(ZipOutputStream zipOutputStream){
            this.zipOutputStream = zipOutputStream;
        }


        @Override
        public void write(int b) throws IOException {
            zipOutputStream.write(b);
        }

        @Override
        public void close() throws IOException {
            //重写此方法来阻止zip中的写出流关闭
        }

        public void putNextEntry(String fileName) throws IOException {
            zipOutputStream.putNextEntry(new ZipEntry(fileName));
        }

    }





}
