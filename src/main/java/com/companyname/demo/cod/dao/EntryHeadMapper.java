package com.companyname.demo.cod.dao;

import com.companyname.demo.cod.config.TkMapper;
import com.companyname.demo.cod.domain.EntryHead;
import org.springframework.stereotype.Repository;

@Repository
public interface EntryHeadMapper extends TkMapper<EntryHead> {
}