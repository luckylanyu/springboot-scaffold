package com.companyname.demo.cod.dao;

import com.companyname.demo.cod.config.TkMapper;
import com.companyname.demo.cod.domain.ScheduledBizTaskInfo;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 定时任务信息表 Mapper 接口
 * </p>
 *
 * @author XGL
 * @since 2021-06-09
 */
@Repository
public interface ScheduledTaskInfoMapper extends TkMapper<ScheduledBizTaskInfo> {

}
