package com.companyname.demo.cod.dao;

import com.companyname.demo.cod.config.TkMapper;
import com.companyname.demo.cod.domain.EntryList;
import org.springframework.stereotype.Repository;

@Repository
public interface EntryListMapper extends TkMapper<EntryList> {
}