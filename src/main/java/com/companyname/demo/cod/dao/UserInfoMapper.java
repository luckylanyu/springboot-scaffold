package com.companyname.demo.cod.dao;

import com.companyname.demo.cod.config.TkMapper;
import com.companyname.demo.cod.domain.UserInfo;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author XGL
 * @since 2021-06-03
 */
@Repository
public interface UserInfoMapper extends TkMapper<UserInfo> {

}
