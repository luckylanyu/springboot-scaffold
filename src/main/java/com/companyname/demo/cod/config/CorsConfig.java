package com.companyname.demo.cod.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class CorsConfig {

    @Bean
    public CorsFilter corsFilter() {
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true); // 允许请求携带cookies
        config.addAllowedOrigin("*");// 允许向该服务器提交请求的origin，*表示全部允许。
        config.addAllowedHeader("*");// 允许访问的头信息,*表示全部
        // config.setMaxAge(1800L);// 预检请求的缓存时间（秒），即在这个时间段里，对于相同的跨域请求不会再预检了。以节约性能,但是定位问题慢，浏览器控制台不显示结果
        config.addAllowedMethod("GET");// 允许提交请求的方法，*表示全部允许
        config.addAllowedMethod("POST");
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

}
