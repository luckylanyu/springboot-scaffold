package com.companyname.demo.cod.controller;

import com.companyname.demo.cod.domain.AjaxResult;
import com.companyname.demo.cod.domain.EntryHead;
import com.companyname.demo.cod.service.EntryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@Slf4j
@RequestMapping("/demoEntry")
public class DemoEntryController {
    @Autowired
    private EntryService entryService;

    @GetMapping("/headList")
    public AjaxResult getEntryHeadList(EntryHead entryHead) {
        return entryService.selectEntryHeadPageList(entryHead);
    }

    @GetMapping("/goodsList")
    public AjaxResult getGoodsList(String seqNo) {
        return entryService.selectGoodsListBySeqNo(seqNo);
    }

    /**
     * excel导出，表头表体在不同sheet页
     */
    @PostMapping("/exportExcelHeadBodyEachSheet")
    public void exportExcelHeadBodyEachSheet(@RequestParam(name = "ids") String ids,
                                             @RequestParam(name = "fileName", required = false) String fileName,
                                             HttpServletResponse response) {
        entryService.exportExcelHeadBodyEachSheet(ids, fileName, response);
    }

    /**
     * excel导出，表头表体在一个sheet页，多sheet页
     */
    @PostMapping("/exportExcelHeadBodySameMultiSheet")
    public void exportExcelHeadBodySameMultiSheet(@RequestParam(name = "ids") String ids,
                                                  @RequestParam(name = "fileName", required = false) String fileName,
                                                  HttpServletResponse response) {
        entryService.exportExcelHeadBodySameMultiSheet(ids, fileName, response);
    }

    /**
     * excel导出，表头表体在一个sheet页，批量zip压缩导出
     */
    @PostMapping("/exportExcelSameSheetZipBatch")
    public void exportExcelSameSheetZipBatch(@RequestParam(name = "ids") String ids,
                                             @RequestParam(name = "fileName", required = false) String fileName,
                                             HttpServletResponse response) {
        entryService.exportExcelSameSheetZipBatch(ids, fileName, response);
    }


}
