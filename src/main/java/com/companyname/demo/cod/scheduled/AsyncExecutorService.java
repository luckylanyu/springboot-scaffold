package com.companyname.demo.cod.scheduled;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import java.util.Map;

/**
 * 指定某个线程池来异步执行：实现具体的业务逻辑
 */
@Service
@Slf4j
@EnableAsync
public class AsyncExecutorService {

    /**
     * 使用fixedThreadPool线程池来异步处理
     * @param taskObj  任务对象，使用Map类型模拟POJO
     */
    @Async("fixedThreadPool")
    public void fixedThreadPoolAsyncExecute(Map<String, Object> taskObj) throws InterruptedException {
        String taskName = (String) taskObj.get("taskName");
        
        if (taskName.equals("task1")) {
            log.info("开始处理任务{}，计划耗时3s", taskName);
            Thread.sleep(3000L);
        } else if (taskName.equals("task2")) {
            log.info("开始处理任务{}，计划耗时5s", taskName);
            Thread.sleep(5000L);
        } else if (taskName.equals("task3")) {
            log.info("开始处理任务{}，计划耗时6s", taskName);
            Thread.sleep(6000L);
        } else if (taskName.equals("task4")) {
            log.info("开始处理任务{}，计划耗时15s", taskName);
            Thread.sleep(15000L);
        } else if (taskName.equals("task5")) {
            log.info("开始处理任务{}，计划耗时60s", taskName);
            Thread.sleep(60000L);
        }
        log.info("任务{}执行完成", taskName);
    }





}
