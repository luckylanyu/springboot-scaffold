package com.companyname.demo.cod.scheduled;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

@Slf4j
public class CustomScheduledFutureTask extends FutureTask<Object> {
    public CustomScheduledFutureTask(Callable<Object> callable) {
        super(callable);
    }

    //任务唯一键，任务的唯一标识
    private String taskId;
    private String taskName;

    /**
     * queue里任务执行时的回调方法，具体业务逻辑
     */
    @Override
    public void run() {
        log.info("开始执行{}任务", this.taskName);

        //...执行任务的业务逻辑，按taskName调用不同的业务逻辑方法
        try {
            Thread.sleep(25000);//测试用，任务耗时25s
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("********{}任务执行完毕！*********", this.taskName);
    }


    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}
